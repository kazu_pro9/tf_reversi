# coding: utf-8
""" DQNエージェントの自己対戦学習を行うスクリプトモジュールを提供する。 """

from player.dqn_agent import DQNAgent
import numpy as np
import copy
import game.reversi as game
import argparse

def swap(players) :
    return [players[1], players[0]]

def avg(qvalues):
    list = np.array(qvalues)
    return np.mean(list)

def train(target_qvalue : float, player1_model_dir : str, player2_model_dir : str):
    """
    DQNエージェントの自己対戦学習を行う。
    @param target_qvalue 目標とするQ値
    @param player1_model_dir プレーヤー1のモデルパス
    @param player2_model_dir プレーヤー2のモデルパス
    """
    # 事前準備
    env = game.Reversi()
    
    
    player1 = DQNAgent('player1', game.ROWS, game.COLS, game.CELLS)
    if not player1_model_dir is None:
        player1.load_model(player1_model_dir)
        
    player2 = DQNAgent('player2', game.ROWS, game.COLS, game.CELLS)
    if not player2_model_dir is not None:
        player2.load_model(player2_model_dir)
        
    players = [player1, player2]
    stones = [game.Stone.Black, game.Stone.White]
        
    # 記録用変数
    player1_qval = []
    player1_mean_qval = 0.
    epoch = 0
    
    # 学習
    while(player1_mean_qval <= target_qvalue):
        # ゲーム開始準備
        epoch = epoch + 1
        
        if epoch != 1 and epoch % 1 == 1:
            reversed(players)
        
        env.reset()
        
        # ゲーム開始
        while not env.is_end():
            
            for player, stone in zip(players, stones):
                board = copy.deepcopy(env.board)
                enables = env.get_enables(env.phase)
                
                if len(enables) > 0:
                    qval, action = player.select_train_action(env)
                    
                    next_env = copy.deepcopy(env)
                    next_env.update(next_env.phase, action)
                    next_env.change_player()
                    
                    winner, black_score, white_score = next_env.winner() 
                    
                    next_board = copy.deepcopy(next_env.board)
                    next_enables = next_env.get_enables(next_env.phase)
                    if len(next_enables) == 0:
                        next_env.change_player()
                        next_enables = next_env.get_enables(next_env.phase)
                        
                    for train_player in players:
                        reword = 0
                        if next_env.is_end():
                            if winner == stone:
                                reword = 1
                            else:
                                reword = -1
                                
                        train_player.store_experience(
                            board, enables, action, reword, next_board, next_enables, next_env.is_end()
                        )
                        train_player.experience_replay()
                        
                    env.update(env.phase, action)    
                    print(
                        "player:{} | action:{:6} | pos:{:2d} | LOSS: {:.4f} | Q_MAX: {:.4f}".format(
                            player.player_name, player.action_name, action, player.train_loss, player.train_qval
                        )
                    )
                    
                    if player.player_name == 'player1':
                        player1_qval.append(player.train_qval)
                    
                env.change_player()

        # ゲーム終了
        winner, black_score, white_score = env.winner()
        print(
            "EPOCH: {:07d} | WIN: player{} 黒：{:2} 白:{:2}".format(
                epoch, winner.name, black_score, white_score
            )
        )
        
        # Q値の平均算出
        player1_mean_qval = avg(player1_mean_qval)
        
    player1.save_model('finished')
    
          
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target_qvalue", required=True)
    parser.add_argument("-p1", "--player1_model_dir", required=False)
    parser.add_argument("-p2", "--player2_model_dir", required=False)
    args = parser.parse_args()
    
    train( float(args.target_qvalue), args.player1_model_dir, args.player2_model_dir)