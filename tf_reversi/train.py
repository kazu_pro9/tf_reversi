# coding: utf-8
""" モデルの学習に関するモジュールを提供する。 """
from player.agent import Agent
from player.player_factory import create_agent
from player.dqn_agent import DQNAgent
from tools.train_dqn
import game.reversi as game

import argparse
# import agent.agent_factory as factory

def main(agent_type : str, agent_name : str):
    """
    学習を行うメイン。
    @param agent_type as str エージェント種別
    @param agent_name as str エージェント名
    """
    if agent_type == DQNAgent.TYPE:
        print('DQNAgentの学習を行うには train_dqn.py を実行してください。')
        return
        
    agent = create_agent(agent_type, agent_name, game.ROWS, game.COLS, game.CELLS)
    agent.train()
    agent.save_model()
    
if __name__ == "__main__":
    # 引数パーサー
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--agent_type", required=True)
    parser.add_argument("-n", "--agent_name", required=True)
    args = parser.parse_args()
    
    main(args.agent_type, args.agent_name)