# conding: utf-8
""" 棋譜データセットに関するモジュールを提供する。 """

from game.reversi import GameLoader
from game.reversi import Stone
from game.reversi import CELLS
import util.path_util as path_util
import util.csv_util as csv_util
import numpy as np

class GameRecordSet:
    """ 棋譜データセットクラスを提供する。 """
    
    # クラス変数 *****************************************************
    __TRAIN_FILE = 'train_sets.csv'             # 訓練データファイル名
    __VALIDATION_FILE = 'validation_sets.csv'   # 検証データファイル名
    __TEST_FILE = 'test_sets.csv'               # テストデータファイル名
    # **************************************************************
    
    def __init__(self):
        """ コンストラクタ """
        # ファイルパス
        self.train_sets_file = self.__get_train_sets_filepath()
        self.validation_sets_file = self.__get_validation_sets_filepath()
        self.test_sets_file = self.__get_test_sets_filepath()
        
        # データセットの初期化
        self.train_data = []
        self.validation_data = []
        self.test_data = []
        
        self.validation_boards = None
        self.validation_stones = None
        self.validation_cells = None
        
        self.test_boards = None
        self.test_stones = None
        self.test_cells = None
        
        self.load()
        
    def load(self):
        """ 
        棋譜データを読み込み学習データ・検証データ・テストデータに分割する。 
        """
        if self.__exists_trainset():
            self.train_data = self.__load_sets_file(self.train_sets_file)
            self.validation_data = self.__load_sets_file(self.train_sets_file)
            self.test_data = self.__load_sets_file(self.train_sets_file)
        else:
            self.train_data, self.validation_data, self.test_data = self.__generate()
            
        
    def sampling(self, size : int) -> ([np.ndarray], [int], [int]) :
        """
        訓練データセットからサンプリングする。
        @param size as int 取得するデータ数
        @return 棋譜データ( タプル(盤面, 石, セル番号)のリスト )
        """
        min_size = min(size, len(self.train_data))
        indexes = np.random.choice(len(self.train_data), min_size)
        records = [self.train_data[index] for index in indexes]
        
        boards, stones, cells = self.collect(records)
        return boards, stones, cells

        
    def get_validation_sets(self) -> ([np.ndarray], [int], [int]) :
        """
        検証データセットを取得する。
        @return 棋譜データ( タプル(盤面, 石, セル番号)のリスト )
        """
        if self.validation_boards is None or self.validation_stones is None or self.validation_cells is None:
            self.validation_boards, self.validation_stones, self.validation_cells = self.collect(self.validation_data)
            
        return self.validation_boards, self.validation_stones, self.validation_cells
        
    def get_test_sets(self) -> ([np.ndarray], [int], [int]) :
        """
        テストデータセットを取得する。
        @return 棋譜データ( タプル(盤面, 石, セル番号)のリスト )
        """
        if self.test_boards is None or self.test_stones is None or self.test_cells is None:
            self.test_boards, self.test_stones, self.test_cells = self.collect(self.test_data)
            
        return self.test_boards, self.test_stones, self.test_cells
        
    def __exists_trainset(self) -> bool:
        """
        学習データセットが存在しているかチェックする。
        @return 有無
        """
        if not path_util.exists(self.train_sets_file):
            return False
        
        if not path_util.exists(self.validation_sets_file):
            return False
        
        if not path_util.exists(self. test_sets_file):
            return False
        
        return True
        
    def __get_train_sets_filepath(self) -> str:
        """
        訓練データセットファイルのパスを取得する。
        @return 訓練データセットファイルのパス
        """
        result = path_util.get_train_file(GameRecordSet.__TRAIN_FILE)
        return result
    
    def __get_validation_sets_filepath(self) -> str:
        """
        検証データセットファイルのパスを取得する。
        @return 検証データセットファイルのパス
        """
        result = path_util.get_train_file(GameRecordSet.__VALIDATION_FILE)
        return result
    
    def __get_test_sets_filepath(self) -> str:
        """
        テストデータセットファイルのパスを取得する。
        @return テストデータファイルのパス
        """
        result = path_util.get_train_file(GameRecordSet.__TEST_FILE)
        return result
    
    def __load_sets_file(self, file_path) -> [(np.ndarray, Stone, int)] :
        """
        訓練データセットファイルを読み込む。
        @param 訓練データセットファイル
        @return 訓練データセット as [(盤面, 石, セルID)]
        """
        loader = GameLoader(file_path)
        return loader.records
    
    def __generate(self) -> ([np.ndarray, Stone, int], [np.ndarray, Stone, int], [np.ndarray, Stone, int]) :
        """
        データセットを生成する。
        棋譜ファイルを訓練データ7割/検証データ2割/テストデータ1割に分割する。
        @return タプル(訓練データセット, 検証データセット, テストデータセット)
        """
        game_records_dir = path_util.get_game_record_dir()
        
        files = path_util.enumerate_files(game_records_dir)
        np.random.shuffle(files)
        
        train_file_lats_index = int(len(files) * 0.7)
        valiation_file_last_index = int(len(files) * 0.9)
        
        # 訓練データセット
        train_data = []
        for file in files[:train_file_lats_index]:
            records = self.__load_sets_file(file)
            train_data.extend(records)
        self.__save(self.train_sets_file , train_data)
            
        # 検証データセット
        validation_data = []
        for file in files[train_file_lats_index:valiation_file_last_index]:
            records = self.__load_sets_file(file)
            validation_data.extend(records)
        self.__save(self.validation_sets_file , validation_data)
        
        # テストデータセット
        test_data = []
        for file in files[valiation_file_last_index:]:
            records = self.__load_sets_file(file)
            test_data.extend(records)
        self.__save(self.test_sets_file , test_data)
        
        return train_data, validation_data, test_data
    
    def __save(self, file_path : str, contents : [(np.ndarray, Stone, int)]):
        """
        データセットをファイルに保存する。
        @param file_path as str ファイルパス
        @param contents データセット
        """
        lines = []
        
        for content in contents:
            line = content[0].flatten().tolist()
            line.append(content[1].value)
            line.append(content[2])
            lines.append(line)
        
        csv_util.write_all_lines(file_path, lines)
        
    def collect(self, datasets : [(np.ndarray, Stone, int)]) -> ([np.array], [Stone], [int]) :
        """
        学習用にデータセットを加工する。
        """
        boards = []
        stones = []
        cells = []
        for record in datasets:
            boards.append(record[0])
            stones.append(record[1])
            
            cell = record[2]
            expected_list = np.zeros(len(CELLS))
            expected_list[cell] = 1.0
            cells.append(expected_list)

        return boards, stones, cells