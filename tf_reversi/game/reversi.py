# encoding: utf-8
""" リバーシゲームに関するモジュールを提供する。 """

from enum import IntEnum
from datetime import datetime
import util.path_util as path_util
import util.csv_util as csv_util
import numpy as np
import copy


# *********************************************************************************
# 定数定義
# 盤面サイズ定義
ROWS = 6 # 行数
COLS = 6 # 列数
CELLS = np.arange(ROWS * COLS)

PASS = -1 # パス
# *********************************************************************************
class Stone(IntEnum):
    """ 石の列挙を提供する """
    # 空
    Empty = 0
    # 黒
    Black = 1
    # 白
    White = 2
    
    def is_empty(self) -> bool :
        """
        空であるかを判定する。
        @return 可否
        """
        return self == Stone.Empty

    def is_black(self) -> bool :
        """
        黒であるかを判定する。
        @return 可否
        """
        return self == Stone.Black

    def is_white(self) -> bool :
        """
        白であるかを判定する。
        @return 可否
        """
        return self == Stone.White

    def is_self(self, other) -> bool :
        """
        自分の石であるかを判定する。
        @return 可否
        """
        return self == other

    def is_enemy(self, other) -> bool :
        """
        相手の石であるかを判定する。
        @return 可否
        """
        return self != other

# *********************************************************************************
class Attack:
    """ 手に関するクラスを提供する """
    
    def __init__(self, stone : Stone, cell : int, turnover_cells : [int]):
        """
        コンストラクタ
        @param stone 石
        @param cell マス
        @param turnover_cells ひっくり返す石の位置
        """
        self.stone = stone
        self.cell = cell
        self.turnover_cells = turnover_cells
        self.count = len(self.turnover_cells)
        
# *********************************************************************************
class AttackCache:
    """ 手に関するキャッシュクラスを提供する """
    
    def __init__(self):
        """
        コンストラクタ
        """
        self.clear()
        
    def clear(self):
        """
        キャッシュのクリアを行う。
        """
        self.cache = { Stone.Black:{}, Stone.White:{} }
        
    def update(self, stone : Stone, cell : int, attack : Attack):
        """
        キャッシュを更新する。
        @param stone 石
        @param cell マス
        @param attack 手
        """
        self.cache[stone].update({cell:attack})
               
    def get_attack(self, stone : Stone, cell : int) -> Attack:
        """
        キャッシュ情報を取得する。
        @param stone 石
        @param cell マス
        @return 手
        """
        return self.cache[stone][cell]
    
    def get_enables(self, stone : Stone) -> [int]:
        """
        有効手の一覧を取得する。
        @param stone 石
        @return 有効手のリスト
        """
        return list(self.cache[stone].keys())
    
# *********************************************************************************
class GameRecorder:
    """ 棋譜記録を提供する """
    
    def __init__(self):
        """
        コンストラクタ
        """
        self.records = []
        self.reset()
        
    def reset(self):
        """
        初期化を行う。
        """
        if len(self.records) != 0:
            self.save()
        self.records = []
        
    def record(self, board : np.ndarray, stone : Stone, cell : int):
        """
        局面・手を記録する。
        @param board 盤面
        @param stone 石
        @param cell マス
        """
        line = board.flatten().tolist()
        line.append(stone.value)
        line.append(cell)
        self.records.append(line)
    
    def save(self, file_name):
        """
        ファイルに保存する。
        @param file_name ファイル名
        """
        file_path = path_util.get_game_record_file(file_name, '.csv')
        csv_util.write_csv(file_path, self.records)

# *********************************************************************************
class GameLoader :
    """ 棋譜を読み込む """
    
    def __init__(self, file_path):
        """
        コンストラクタ
        @param file_path ファイルパス
        """
        self.records = []
        self.current_index = -1
        self.file_path = file_path
        self.load(self.file_path)
        
    def load(self, file_path):
        """
        棋譜ファイルを読み込む。
        @param file_path ファイルパス
        """
        tmp_records = csv_util.load_csv(file_path)
        for record in tmp_records:
            self.records.append(self.__to_record(record))
        self.current_index = 0
        
    def get_current(self) -> (np.ndarray, Stone, int) :
        """
        現在局面を取得する。
        @return 盤面, 石
        """
        result = self.records[self.current_index]
        return result
        
    def is_next(self) -> bool:
        """
        手を1つ進めることができるか判定する。
        @return 可否
        """
        return self.current_index < len(self.records)
        
    def move_next(self):
        """
        手を進める。
        """
        self.current_index = self.current_index + 1
        
    def is_prev(self) -> bool :
        """
        手が1つ前に戻ることができるか判定する。
        @return 可否
        """
        return  0 < self.current_index
        
    def move_prev(self) :
        """
        手を1つ戻す。
        """
        self.current_index = self.current_index - 1
        
    def __to_record(self, line) -> (np.ndarray, Stone, int) :
        """
        棋譜の行を局面情報に変換する。
        @return 盤面, 石, 手
        """
        flat_board = [float(col) for col in line[:-2]]
        board = np.reshape(flat_board, (ROWS, COLS))
        
        stone_val = int(line[len(line) - 2])
        stone = Stone(stone_val)
        
        cell = int(line[len(line) -1])
        
        return board, stone, cell

# *********************************************************************************
class Reversi:
    """ リバーシゲームを提供する """
    
    def __init__(self):
        """
        コンストラクタ
        """
        self.recorder = None
        self.loader = None
        self.reset()
        
    def reset(self):
        """
        盤面を初期化する。
        """
        self.recorder = GameRecorder()
        self.board = np.zeros((ROWS, COLS))
        for default_pos in self.__get_default_pos():
            self.__set_cell(default_pos[0], default_pos[1], default_pos[2])
            
        self.cache = AttackCache()
        self.phase = Stone.Black
        self.__recache()
        
    def change_player(self):
        """
        手番を交代する。
        """
        self.phase = Stone.White if self.phase.is_black() else Stone.Black
            
    def update(self, stone : Stone, cell : int, attack=None, recache=True) :
        """
        石を置き盤面を更新する。
        @param stone 石
        @param cell マス
        @param attack 手
        @param recache 有効手のキャッシュを更新するかの可否
        """
        if self.is_pass(cell):
            return 0
        
        if attack is None:
            attack = self.cache.get_attack(stone, cell)
            
        if attack.count > 0:
            copy_board = copy.deepcopy(self.board)
            
            for turnover_cell in attack.turnover_cells:
                self.set_cell(turnover_cell, stone)
            self.set_cell(cell, stone)
            
            if not self.recorder is None:
                self.recorder.record(copy_board, stone, cell)
        
        if recache:
            self.__recache()
        return attack.count
    
    def get_enables(self, stone : Stone) -> [int] :
        """
        石を置ける位置を取得する。
        @param stone 石
        @return 有効手のリスト
        """
        return self.cache.get_enables(stone)
    
    def is_end(self) -> bool :
        """
        終局かどうかを判定する。
        @return 可否
        """
        black_enables = self.get_enables(Stone.Black)
        white_enables = self.get_enables(Stone.White)
        
        if len(black_enables) == 0 and len(white_enables) == 0:
            return True
        
        for cell in CELLS:
            if self.get_cell(cell).is_empty():
                return False
        return True
    
    def winner(self) -> (Stone, int, int) :
        """
        勝敗結果を取得する。
        @return 勝者の石, 先手のスコア, 後手のスコア
        """
        black_score = self.get_score(Stone.Black)
        white_score = self.get_score(Stone.White)
        
        if black_score > white_score :
            return (Stone.Black, black_score, white_score)
        elif black_score < white_score :
            return (Stone.White, black_score, white_score)
        else:
            return (Stone.Empty, black_score, white_score)
    
    def get_score(self, stone : Stone) -> int :
        """
        スコアを取得する。
        @param stone 石
        @return スコア
        """
        def func(cell_id) -> bool :
            return self.get_cell(cell_id).is_self(stone)
        
        vfunc = np.vectorize(func)
        result = len(CELLS[vfunc(CELLS)])
        return result
    
    def is_pass(self, cell : int) -> bool :
        """
        パスを示す手であるか判定する。
        @param cell セルID
        @return 可否
        """
        return (cell == PASS)
    
    def save(self, file_name : str) :
        """
        棋譜を保存する。
        @param file_name ファイル名
        """
        self.recorder.save(file_name)
        
    def load(self, file_path : str):
        """
        棋譜を読み込む。
        @param file_path ファイルパス
        """
        self.reset()
        self.recorder = None
        self.loader = GameLoader(file_path)
        
        current_board, stone, cell_id = self.loader.get_current()
        self.board = current_board
        self.__recache()
        
    def is_move_next(self) -> bool:
        """
        次の局面へ進められるか判定する。
        @return 可否
        """
        return self.loader.is_next()
    
    def move_next(self) :
        """
        次の局面へ進める。
        """
        _, stone, cell = self.loader.get_current()
        attack = self.__try_put(stone, cell)
        self.update(stone, cell, attack)
        
        if self.is_move_next():
            self.loader.move_next()
    
    def is_move_prev(self) -> bool:
        """
        前の局面へ戻せるか判定する。
        @return 可否
        """
        return self.loader.is_prev()
    
    def move_prev(self):
        """
        前の局面へ戻す。
        """
        if not self.is_move_prev():
            return
            
        self.loader.move_prev()
        prev_board, stone, cell = self.loader.get_current()
        self.board = prev_board
        self.__recache()
            
    def print_screen(self) :
        """
        盤面を標準出力へ出力する。
        """
        for row in range(ROWS):
            line = ''
            
            for col in range(COLS):
                cell = self.__to_cell(row, col)
                cell_val = self.__get_cell(row, col)
                
                if cell_val.is_empty() :
                    line = line + ' ' + '{0:2d}'.format(cell)
                elif cell_val.is_black():
                    line = line + ' ' + '●'
                elif cell_val.is_white():
                    line = line + ' ' + '○'
            print(line)
        
    def get_cell(self, cell : int) -> Stone:
        """
        指定マスから石を取得する。
        @param cell マス
        @return 石
        """
        pos = self.__to_pos(cell)
        return self.__get_cell(pos[0], pos[1])
    
    def set_cell(self, cell : int, stone : Stone):
        """
        マスに石を設定する。
        @param cell マス
        @param stone 石
        """
        pos = self.__to_pos(cell)
        self.__set_cell(pos[0], pos[1], stone)
        
    def __get_cell(self, row : int, col : int) -> Stone :
        """
        指定マスから石を取得する。
        @param 行
        @param 列
        @return 石
        """
        cell_val = self.board[row][col]
        return Stone(cell_val)
    
    def __set_cell(self, row : int, col : int, stone : Stone):
        """
        マスに石を設定する。
        @param row 行
        @param col 列
        @param stone 石
        """
        self.board[row][col] = stone

    def __to_pos(self, cell : int) -> (int, int) :
        """
        マスIDを行列に変換する。
        @param cell マス
        @return 行,列
        """
        row = int(cell // ROWS)
        col = int(cell % COLS)
        return (row, col)
    
    def __to_cell(self, row : int, col : int) -> int :
        """
        行列からマスIDに変換する。
        @param row 行
        @param col 列
        @return マス
        """
        cell = row * ROWS + col
        return cell
    
    def __get_default_pos(self) : 
        """
        初期配置位置と配置する石の組み合わせを取得する。
        """
        mid_def = np.array([ROWS // 2, COLS // 2, 0])
        pos_defs = np.array([(-1, -1, Stone.White), 
                             (-1, 0, Stone.Black), 
                             (0, -1, Stone.Black), 
                             (0, 0, Stone.White)])
        
        return pos_defs + mid_def
    
    def __try_put(self, stone : Stone, cell : int) -> Attack:
        """
        マスに石を置きひっくり返す石の位置情報等を取得する。盤面の更新は行わない。
        @param stone 石
        @param cell マス
        @return 手を打った後の情報
        """
        if not self.get_cell(cell).is_empty():
            return Attack(stone, cell, [])
        
        pos = self.__to_pos(cell)
        x = pos[1]
        y = pos[0]
        turnover_poses = [] # ひっくり返す石の位置リスト
        
        rel_cols = [-1, 0, 1] # 左右の相対位置(cellベース)
        rel_rows = [-ROWS, 0, ROWS] # 上下の相対位置(cellベース)
        col_list = [x, (COLS - 1), (COLS -1 - x)] # [列番号, 最大列番号, 自身より右側の列数]
        row_list = [y, (ROWS -1), (ROWS -1 -y)] # [行番号, 最大行番号, 自身より下側の行数]
        
        for rel_col, col in zip(rel_cols, col_list):
            for rel_row, row in zip(rel_rows, row_list):
                
                if rel_col == rel_row == 0:
                    continue
                
                enemy_stones = 0
                turnover_poses_temp = []
                
                target_cells = CELLS[cell + rel_col + rel_row :: rel_col + rel_row][:min(col, row)]
                for target_cell in target_cells:
                    target_stone = self.get_cell(target_cell)
                    if target_stone.is_empty():
                        break
                    elif target_stone.is_self(stone):
                        turnover_poses.extend(turnover_poses_temp) # ひっくり返す石(位置)を確定する
                        break
                    else:
                        turnover_poses_temp.insert(0, target_cell) # ひっくり返す位置をストックする
                
        result = Attack(stone, cell, turnover_poses)
        return result
    
    def __recache(self):
        """
        各プレイヤーの有効手をキャッシュしなおす。
        """
        self.cache.clear()
        
        for stone  in [Stone.Black, Stone.White]:
            for cell in CELLS:
                if not self.get_cell(cell).is_empty():
                    continue

                attack = self.__try_put(stone, cell)
                if attack.count > 0:
                    self.cache.update(stone, cell, attack)
