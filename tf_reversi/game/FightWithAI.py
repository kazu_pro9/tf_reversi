
import argparse

import reversi as game
from mlp_agent import MLPAgent
from cnn_agent import CNNAgent

if __name__ == "__main__":
    # args
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model_path")
    parser.add_argument("-s", "--save", dest="save", action="store_true")
    parser.set_defaults(save=False)
    args = parser.parse_args()

    # environmet, agent
    env = game.Reversi()
    
    # agent = MLPAgent()
    # agent.load_model('E:\\git\\tf-dqn-reversi\\model\\MLPAgent\20171016185810')
    agent = CNNAgent('CNNAgent')
    agent.load_model('E:\\git\\tf-dqn-reversi\\model\\CNNAgent\\20171024145020')

    # game
    print("------------- GAME START ---------------")
    while not env.is_end():
        print("*** userターン○ ***")
        env.print_screen()
        enables = env.get_enables(game.Stone.Black)
        if len(enables) > 0:
            flg = False
            while not flg:
                print("番号を入力してください")
                print(enables)
                inp = input('>>>  ')
                action_t = int(inp)
                for i in enables:                
                    if action_t == i:
                        flg = True                       
                        break
                
            env.update(game.Stone.Black, action_t)
        else:
            print("パス")
            
            
        if env.is_end() == True:break
            
        print("*** AIターン● ***")
        env.print_screen()
        enables = env.get_enables(game.Stone.White)
        if len(enables) > 0:
            qvalue, action_t = agent.select_enable_action(env.board, enables)
            print('>>>  {:} / accuracy:{}'.format(action_t, qvalue))              
            env.update(game.Stone.White, action_t)
        else:
            print("パス")


    print("*** ゲーム終了 ***")
    env.print_screen()
    
    win = env.winner()
    if win[0] == game.Stone.Black:
        print("あなたの勝ち！ 黒:{:} / 白:{:}".format(win[1], win[2]))
    elif win[0] == game.Stone.White:
        print("あなたの負け！ 黒:{:} / 白:{:}".format(win[1], win[2]))
    else:
        print("ひきわけ 黒:{:} / 白:{:}".format(win[1], win[2]))
    
