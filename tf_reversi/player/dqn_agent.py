# coding: utf-8
""" Deep Q Network を使用したエージェントを提供する。 """

from player.agent import Agent
from collections import deque
import tensorflow as tf
import numpy as np
import game.reversi as game
import util.path_util as path_util

class ReplayMemory:
    """
    学習データ蓄積のメモリークラスを提供する。
    """
    
    def __init__(self, min_memory_size : int, max_memory_size : int):
        """
        コンストラクタ
        @param min_memory_size 最小蓄積データ量
        @param max_memory_size 最大蓄積データ量
        """
        self.min_memory_size = min_memory_size
        self.max_memory_size = max_memory_size
        self.memory = deque(maxlen=self.max_memory_size)
        
    def is_less_than_min_size(self):
        """
        最小蓄積量未満であるか判定する。
        @return 可否
        """
        return len(self.memory) < self.min_memory_size
    
    def is_greater_than_max_size(self):
        """
        最大蓄積量より大きいかを判定する。
        @return 可否
        """
        return len(self.memory) > self.max_memory_size
        
        
    def add(self, state : np.ndarray, targets : [int], action : int, reward : int, \
            state_1 : np.ndarray, targets_1 : [int], terminal : bool):
        """
        学習データを追加する。
        @param state 盤面
        @param targets 有効手一覧
        @param action 手
        @param reward　報酬
        @param state_1 次の盤面(選択した手を打った後に相手も手を打った後)
        @param targets_1 次の有効手一覧
        @param terminal 終局判定
        """
        self.memory.append((state, targets, action, reward, state_1, targets_1, terminal))
        if self.is_greater_than_max_size():
            self.D.popleft()

    def sample(self, size : int) -> [(np.ndarray, [int], int, int, np.ndarray, [int], bool)]:
        """
        サンプリングを行う。
        @size サンプル数
        @return (盤面, 有効手一覧, 手, 次の盤面, 次の有効手一覧, 終局判定)
        """
        result = []
        for index in np.random.choice(len(self.memory), size):
            result.append(self.memory[index])
        return result

class DQNAgent(Agent):
    """ Deep Q Network を使用したエージェントクラスを提供する。 """
    
    # **************************************************************************
    # エージェント共通
    TYPE = 'DQN'                 # エージェント種別
    
    # ネットワークパラメーター
    FC1_NEURON = 288             # 隠れ層1(全結合層)のニューロン数 ※要調整
    FC2_NEURON = 144             # 隠れ層2(全結合層)のニューロン数 ※要調整
    
    # ε-greedy法パラメーター
    INITIAL_EPSILON = 1.0        # ε-greedy法のεの初期値
    FINAL_EPSILON = 0.1          # ε-greedy法のεの終値
    EXPLORATION_STEPS = 160000   # ε-greedy法のεが減少していく手数 ※ 要調整
    
    # 学習データ蓄積量
    REPLAY_MEMORY_SIZE = 160000  # 手の最大蓄積量 ※ 要調整
    MIN_REPLAY_SIZE = 16000      # 手の最低蓄積量 ※ 要調整
    
    # Q学習
    DISCOUNT_FACTOR = 0.9        # 割引率 ※ 要調整
    
    # 行動名
    RANDOM_ACTION = 'random'     # ランダム行動の名称
    AI_ACTION = 'ai    '         # 最適行動の名称
    
    # 学習パラメーター
    LEARNING_RATE = 0.001        # 学習率 ※要調整
    MINIBATCH_SIZE = 32         # 学習時のサンプリング数 ※要調整
    
    # 保存頻度
    SAVE_STEP = 100              # 指定STEP毎に保存 ※ 要調整
    # **************************************************************************
    
    def __init__(self, agent_name : str, rows : int, cols : int, cells : np.array):
        """
        コンストラクタ
        @param agent_type as str      エージェント種別(MLP, CNN, DQN ...etc)
        @param agent_name as str      エージェント名
        @param rows       as int      盤面の行数
        @param cols       as int      盤面の列数
        @param cells      as nd.array 盤面上のマス一覧
        """
        super(DQNAgent, self).__init__(DQNAgent.TYPE, agent_name, rows, cols, cells)
        self.init_model()
        self.init_train_params()
        
    def init_train_params(self):
        """
        学習用パラメーターの初期化を行う。
        """
        # 学習情報
        self.action_name = ''
        self.train_step = 0
        self.train_loss = 0.
        self.train_qval = 0.
        self.train_action = 0
        
        # 学習データ蓄積メモリー
        self.memory = ReplayMemory(self.MIN_REPLAY_SIZE, self.REPLAY_MEMORY_SIZE)
        
        # ε-greedy法
        self.epsilon = self.INITIAL_EPSILON
        self.epsilon_step = (self.INITIAL_EPSILON - self.FINAL_EPSILON) / self.EXPLORATION_STEPS
        
    def init_model(self):
        """
        モデルを初期化する。
        """
        # Session
        self.sess = tf.InteractiveSession()
        
        # 入力層
        with tf.name_scope('input'):
            self.input_boards = tf.placeholder(tf.float32, [None, self.rows, self.cols], name='board')
            flat_boards = tf.reshape(self.input_boards, [-1, self.n_cells], name='flat_board')
            
        # 隠れ層1(全結合層)
        # 活性化関数: ReLU関数
        with tf.name_scope('FC1'):
            fc1_weight = tf.Variable(tf.truncated_normal([self.n_cells, self.FC1_NEURON], stddev=0.01), name='fc1_widght')
            fc1_bias = tf.Variable(tf.zeros([self.FC1_NEURON]), name='fc1_bias')
            fc1 = tf.nn.relu(tf.add(tf.matmul(flat_boards, fc1_weight), fc1_bias), name='fc1')
            
        # 隠れ層2(全結合層)
        # 活性化関数: ReLU関数
        with tf.name_scope('FC2'):
            fc2_weight = tf.Variable(tf.truncated_normal([self.FC1_NEURON, self.FC2_NEURON], stddev=0.01), name='fc2_widght')
            fc2_bias = tf.Variable(tf.zeros([self.FC2_NEURON]), name='fc2_bias')
            fc2 = tf.nn.relu(tf.add(tf.matmul(fc1, fc2_weight), fc2_bias), name='fc2')
            
        # 出力層
        # 活性化関数: softmax関数
        with tf.name_scope('output'):
            out_Weight = tf.Variable(tf.truncated_normal([self.FC2_NEURON, self.n_cells], stddev=0.01), name='output_weight')
            out_bias = tf.Variable(tf.zeros([self.n_cells]), name='output_bias')
            self.out = tf.add(tf.matmul(fc2, out_Weight), out_bias, name='output')
            self.out_sigmoid = tf.nn.sigmoid(self.out, name='output_sigmoid')
            self.out_softmax = tf.nn.softmax(self.out, name='output_softmax')
            
        # Policy Network に対する期待値ラベル
        with tf.name_scope('expected'):
            self.expected = tf.placeholder(tf.float32, [None, self.n_cells])
            
        # Policy Network に対する損失関数
        # 損失関数: 最小二乗法
        with tf.name_scope('policy_loss'):
            self.loss = tf.reduce_mean(
                tf.square(self.expected - self.out)
            )
            
        # Policy Network への学習オペレーション
        with tf.name_scope('train'):
            self.training = tf.train.RMSPropOptimizer(self.LEARNING_RATE).minimize(self.loss)
            
        # TensorBoard
        log_dir = path_util.get_log_dir(DQNAgent.TYPE, self.player_name)
        
        with tf.name_scope('summary'):
            tf.summary.scalar('loss', self.loss)
            self.merged = tf.summary.merge_all()
            self.writer = tf.summary.FileWriter(log_dir, self.sess.graph)
            
        # saver
        self.saver = tf.train.Saver()
        
        # initilize session
        self.sess.run(tf.global_variables_initializer())
        
    def select_action(self, board) -> np.ndarray:
        """
        手に対する確率を取得する。
        @param board 盤面
        @return 手に対する確率
        """
        result = self.sess.run(self.out_softmax, feed_dict={self.input_boards: [board]})
        return result[0]
    
    def train(self):
        """
        学習データセットにより学習を行う。
        他エージェントとは学習方法が異なるため実装しない。
        """
        pass
    
    def is_ready(self):
        """
        学習が開始できる準備が整ったか判定する。
        """
        return not self.memory.is_less_than_min_size()
        
    def store_experience(self, state, targets, action, reward, state_1, targets_1, terminal):
        """
        学習データを蓄積する。
        @param state 盤面
        @param targets 有効手一覧
        @param action セル
        @param reward 報酬
        @param state_1 次の盤面
        @param targets_1 次の有効手一覧
        @param terminal 終局判定
        """
        self.memory.add(state, targets, action, reward, state_1, targets_1, terminal)
        
    def experience_replay(self):
        """
        学習データからサンプリングをし学習をする。
        """
        if self.memory.is_less_than_min_size():
            # 学習データが最低量未満のため学習を行わない
            return
        
        self.train_step = self.train_step + 1
        
        state_minibatch = []
        y_minibatch = []
        
        for record in self.memory.sample(self.MINIBATCH_SIZE):
            state_j, targets_j, action_j, reward_j, state_j_1, targets_j_1, terminal = record
            
            y_j = self.sess.run(self.out, feed_dict={self.input_boards: [state_j]})[0]
            if terminal :
                y_j[int(action_j)] = reward_j
            else:
                qvalue, action = self.__select_train_enable_action(targets_j_1, state_j_1)
                y_j[int(action_j)] = reward_j + self.DISCOUNT_FACTOR * np.max(qvalue)
                
            state_minibatch.append(state_j)
            y_minibatch.append(y_j)
        
        # Training
        _, self.train_loss = self.sess.run(
            [self.training, self.loss], 
            feed_dict={self.input_boards: state_minibatch, self.expected: y_minibatch}
        )
        
        # self.writer.add_summary(summary, self.train_step)
        
        if self.train_step % self.SAVE_STEP == 0:
            self.save_model('tmp')
    
    def select_train_action(self, env : game.Reversi) -> (float, int):
        """
        学習時の手の選択を行う。
        @param env リバーシゲーム環境
        @return Q値, セル
        """
        self.train_qval = 1.0
        self.train_action = game.PASS
        
        enables = env.get_enables(env.phase)
        board = env.board
        
        if self.memory.is_less_than_min_size() or np.random.rand() <= self.epsilon:
            # ランダム行動
            self.action_name = self.RANDOM_ACTION
            self.train_qval, self.train_action = self.__select_train_random_action(enables, board)
        else:
            # 最適行動
            self.action_name = self.AI_ACTION
            self.train_qval, self.train_action = self.__select_train_enable_action(enables, board)
            
        # εを線形に減少させる
        self.__update_epsilon()
            
        return self.train_qval, int(self.train_action)
    
    def __select_train_random_action(self, enables : [int], board : np.ndarray) -> (float, int):
        """
        ランダムによる手を選択する。
        ただし学習に多様性を持たせるために最善手を除外する。
        @param enables 有効手一覧
        @param board 盤面
        """
        result_qvalue = 0.
        result_action = game.PASS
        
        if len(enables) == 0:
            return result_qvalue, result_action
        
        qvalues = self.__q_value(board, enables)
        sorted_qvalues = qvalues[ qvalues[:,0].argsort() ] # 先頭の列でソートする
        
        if len(enables) == 1:
            # 有効手が1つしかない場合
            result_qvalue, result_action = sorted_qvalues[len(sorted_qvalues) -1]
            return result_qvalue, result_action
            
        # 最善手を除外する
        best_qvalue, best_action = sorted_qvalues[len(sorted_qvalues) -1]
        enables.remove(best_action)
        
        result_action = np.random.choice(enables)
        for qvalue, action in qvalues:
            if action == result_action:
                result_qvalue = qvalue
                break
        
        return result_qvalue, result_action
    
    def __select_train_enable_action(self, enables : [int], board : np.ndarray) -> (float, int):
        """
        手を選択する。
        有効手が存在しない場合は、精度:1/手:PASS(-1)を返却する。
        @param enables 有効手一覧
        @param board 盤面
        @return Q値, 手のマスID
        """ 
        result_qvalue = 0.
        result_action = game.PASS
        
        if len(enables) == 0:
            return result_qvalue, result_action
        
        qvalues = self.__q_value(board, enables)
        sorted_index = qvalues[ qvalues[:,0].argsort() ] # 先頭の列でソートする
        reversed(sorted_index)
        
        result_qvalue, result_action = sorted_index[len(sorted_index) - 1]
        return result_qvalue, result_action
    
    def __update_epsilon(self):
        """
        epsilon値を更新する。
        """
        if self.epsilon > self.FINAL_EPSILON and self.memory.is_greater_than_max_size():
            self.epsilon -= self.epsilon_step
            if self.epsilon < self.FINAL_EPSILON:
                self.epsilon = self.FINAL_EPSILON
                
    def __q_value(self, board : np.ndarray, enables : [int]) -> np.ndarray:
        """
        Q値を取得する。
        @param board 盤面
        @return 各手に対するQ値
        """
        qvalues = self.sess.run(self.out, feed_dict={self.input_boards: [board]})[0]
        
        result = []
        for enable in enables:
            result.append([qvalues[enable], enable])
        
        return np.array(result)
    