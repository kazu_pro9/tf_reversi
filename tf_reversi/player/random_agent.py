# condig: utf-8
""" 手をランダムで選択するエージェントのモジュールを提供する。 """

from player.player import Player
import game.reversi as game
import numpy as np

class RandomAgent(Player):
    """
    手をランダムで選択するエージェントクラスを提供する。
    """
    
    # クラス変数 *****************************************************************
    TYPE = 'RANDOM'        # エージェント種別
    # ****************************************************************************
    
    def __init__(self, player_name : str):
        """
        コンストラクタ。
        @param player_name プレイヤー名
        """
        super(RandomAgent, self).__init__(RandomAgent.TYPE, player_name)
    
    def train(self):
        """
        学習を行う。
        """
        print('**************** warning ! ****************')
        print("学習を行う必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')
    
    def select_enable_action(self, env : game.Reversi) -> (float, int):
        """
        手を選択する。
        @param env リバーシゲーム環境
        """
        result_action = game.PASS  # PASSを示す-1を設定
        result_accuracy = 1.0        # 人間が手を指定するため精度は1を設定
        
        enables = env.get_enables(env.phase)
        if len(enables) == 0:
            # 有効手なしの場合は、精度:0/手：-1(PASS)を返却する
            return result_accuracy, game.PASS
        
        index = np.random.choice(len(enables), 1)
        result_action = enables[int(index)]
        return result_accuracy, result_action
    
    def is_loadable_model(self):
        """
        モデルの読み込みが可能かを返却する。
        @return 可否
        """
        return False
        
    def load_model(self, model_directory : str):
        """
        モデルを読み込む。 
        @param model_directory モデルの保存先ディレクトリ
        """
        print('**************** warning ! ****************')
        print("モデルを読み込む必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')
        
    def is_savable_model(self):
        """
        モデルが保存可能かを判定する。
        @return 可否
        """
        return False
        
    def save_model(self, sub_dir = None):
        """
        モデルを保存する。
        @param sub_dir サブディレクトリ
        """
        print('**************** warning ! ****************')
        print("モデルの保存が必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')