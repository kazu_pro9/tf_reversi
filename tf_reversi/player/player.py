# coding: utf-8
""" プレイヤーインターフェースに関するモジュールを提供する。 """

from abc import ABC
from abc import abstractmethod
import game.reversi as game

class Player(ABC):
    """ プレイヤーインターフェースを提供する。 """
    
    def __init__(self, player_type : str, player_name : str):
        """
        コンストラクタ。
        継承先クラスにて実装する。
        @param player_type プレーヤー種別
        @param player_name プレーヤー名
        """
        self.player_type = player_type
        self.player_name = player_name
        pass
    
    def __del__(self):
        """
        デストラクタ。
        継承先クラスにて必要に応じて実装する。
        """
        pass
    
    @abstractmethod
    def train(self):
        """
        学習を行う。
        継承先クラスにて実装すること。
        """
        pass
    
    @abstractmethod
    def select_enable_action(self, env : game.Reversi) -> (float, int):
        """
        手を選択する。
        インターフェースとしての実装は、精度:0 / 手：-1(pass)を返却する。
        @param env リバーシゲーム環境
        @return 精度, 手となるマスID
        """
        pass
    
    @abstractmethod
    def is_loadable_model(self):
        """
        モデルの読み込みが可能かを返却する。
        @return 可否
        """
        # デフォルトは False とする
        return False
    
    @abstractmethod
    def load_model(self, model_directory : str):
        """
        モデルを読み込む。 
        @param model_directory モデルの保存先ディレクトリ
        """
        pass
    
    @abstractmethod
    def is_savable_model(self):
        """
        モデルが保存可能かを判定する。
        @return 可否
        """
        # デフォルトは False とする
        return False
        
    @abstractmethod
    def save_model(self, sub_dir : None):
        """
        モデルを保存する。
        @param sub_dir サブディレクトリ
        """
        pass