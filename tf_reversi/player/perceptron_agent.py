# encoding: utf-8
""" パーセプトロンをを使用したエージェントを提供するモジュールです。 """

from game.game_record_set import GameRecordSet
from player.agent import Agent
import util.path_util as path_util
import numpy as np
import tensorflow as tf

class PerceptronAgent(Agent):
    """ パーセプトロンを採用したエージェントクラスを提供する。 """
    
    # クラス変数 *****************************************************************
    TYPE = 'Perceptron'        # エージェント種別
    
    # 下記 要チューニング
    LEARNING_RATE = 0.001     # 学習率(パラメーターの最大更新単位)
    TRAINING_EPOCH = 300000   # 訓練データセットに対する学習回数
    MIN_BATCH_SIZE = 32       # 最小バッチサイズ(訓練データ数)
    DISPLAY_STEP = 100        # ログ出力単位(EPOCH単位)    
    # **************************************************************************
    
    
    def __init__(self, agent_name : str, rows : int, cols : int, cells : np.array):
        """
        コンストラクタ
        @param agent_type as str      エージェント種別(MLP, CNN, DQN ...etc)
        @param agent_name as str      エージェント名
        @param rows       as int      盤面の行数
        @param cols       as int      盤面の列数
        @param cells      as nd.array 盤面上のマス一覧
        """
        super(PerceptronAgent, self).__init__(PerceptronAgent.TYPE, agent_name, rows, cols, cells)
        self.init_model()
               
    def init_model(self):
        """
        モデルを初期化する。
        """
        # Session
        self.sess = tf.InteractiveSession()
        
        # 入力層
        with tf.name_scope('input'):
            self.input_boards = tf.placeholder(tf.float32, [None, self.rows, self.cols], name='board')
        
            # 形状変換
            # [-1, self.rows * self.cols] の -1 は　可変可能な値を示す
            flat_boards = tf.reshape(self.input_boards, [-1, self.n_cells], name='flat_board')
         
        # 出力層
        # activation function: softmax関数(総和1の確率に変換する)
        with tf.name_scope('output'):
            out_Weight = tf.Variable(tf.random_normal([self.n_cells, self.n_cells]), name='output_weight')
            out_bias = tf.Variable(tf.random_normal([self.n_cells]), name='output_bias')
            self.out = tf.add(tf.matmul(flat_boards, out_Weight), out_bias, name='output')
            self.out_softmax = tf.nn.softmax(self.out, name='output_softmax')
        
        # 盤面に対する期待するラベル情報
        with tf.name_scope('expected'):
            self.expected = tf.placeholder(tf.float32, [None, self.n_cells])
        
        # 損失関数(クロスエントロピー)
        with tf.name_scope('loss'):
            self.loss = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(
                    logits=self.out,
                    labels=self.expected
                )
            )
        
        # 学習オペレーション
        # Optimizer: RMSProp(適応的学習率)
        # http://postd.cc/optimizing-gradient-descent/
        with tf.name_scope('train'):
            self.training = tf.train.RMSPropOptimizer(PerceptronAgent.LEARNING_RATE).minimize(self.loss)
        
        # 検証オペレーション        
        with tf.name_scope('acccuracy'):
            self.accuracy = tf.reduce_mean(
                tf.cast(
                    tf.equal(tf.argmax(self.out, 1),  tf.argmax(self.expected, 1)), 
                    tf.float32
                )
            )
        
        # TensorBoard
        log_dir = path_util.get_log_dir(PerceptronAgent.TYPE, self.player_name)
        
        with tf.name_scope('summary'):
            tf.summary.scalar('loss', self.loss)
            tf.summary.scalar('accuracy', self.accuracy)
            self.merged = tf.summary.merge_all()
            self.writer = tf.summary.FileWriter(log_dir, self.sess.graph)
        
        # saver
        self.saver = tf.train.Saver()
        
        # initilize session
        self.sess.run(tf.global_variables_initializer())
        tf.global_variables_initializer().run()
        
    def train(self):
        """
        学習データセットにより学習を行う。
        """
        # Load Training DataSet
        print('load training datasets....................', end="")
        datasets = GameRecordSet()
        valiation_boards, valiation_stones, valiation_cells = datasets.get_validation_sets()
        test_boards, test_stones, test_cells = datasets.get_test_sets()
        print('done')
        
        # Traing
        for epoch in range(PerceptronAgent.TRAINING_EPOCH):
            
            # 学習データセットからサンプリング
            min_batch_board, min_batch_stone, min_batch_cells = datasets.sampling(PerceptronAgent.MIN_BATCH_SIZE)
            
            # Training
            _, train_loss, train_accuracy = self.sess.run(
                [self.training, self.loss, self.accuracy], 
                feed_dict={self.input_boards: min_batch_board, self.expected: min_batch_cells}
            )
            
            if (epoch + 1) % PerceptronAgent.DISPLAY_STEP == 0:
                # Validation
                valid_loss, valid_acc, summary = self.sess.run(
                    [self.loss, self.accuracy, self.merged], 
                    feed_dict={self.input_boards: valiation_boards, self.expected: valiation_cells}
                )

                print(
                    ("epoch:{:05d} " + \
                     "| train_loss:{:10.05f} | train_accuracy:{:06f} " + \
                     "| validation_loss:{:10.05f} | validation_accuracy:{:06f}").format(
                        epoch + 1, train_loss, train_accuracy, valid_loss, valid_acc
                    )
                )
                self.writer.add_summary(summary, epoch + 1)
                
                
        test_loss, test_accuracy = self.sess.run(
            [self.loss, self.accuracy], 
            feed_dict={self.input_boards: test_boards, self.expected: test_cells}
        )
        print("test_loass:{:10.05f} | test_accuracy:{:06f}".format(test_loss, test_accuracy))
        
        
    def select_action(self, board) -> np.ndarray:
        """
        手に対する確立を取得する。
        @param board 盤面
        @return 各マスへの確率
        """
        result = self.sess.run(self.out_softmax, feed_dict={self.input_boards: [board]})
        return result      