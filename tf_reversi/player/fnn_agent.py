# encoding: utf-8
""" FeedForward Neural etworksを使用したエージェントを提供するモジュールです。 """

from game.game_record_set import GameRecordSet
from player.agent import Agent
import util.path_util as path_util
import numpy as np
import tensorflow as tf

class FNNAgent(Agent):
    """ Multi Layer Perceptronを採用したエージェントクラスを提供する。 """
    
    # クラス変数 *****************************************************************
    TYPE = 'FNN'        # エージェント種別
    
    # 下記 要チューニング
    LEARNING_RATE = 0.001     # 学習率(パラメーターの最大更新単位)
    TRAINING_EPOCH = 300000   # 訓練データセットに対する学習回数
    MIN_BATCH_SIZE = 32       # 最小バッチサイズ(訓練データ数)
    DISPLAY_STEP = 100        # ログ出力単位(EPOCH単位)    
    FC1_NEURON = 288          # 第2層(全結合層)のニューロン数
    # **************************************************************************
    
    
    def __init__(self, agent_name : str, rows : int, cols : int, cells : np.array):
        """
        コンストラクタ
        @param agent_type as str      エージェント種別(MLP, CNN, DQN ...etc)
        @param agent_name as str      エージェント名
        @param rows       as int      盤面の行数
        @param cols       as int      盤面の列数
        @param cells      as nd.array 盤面上のマス一覧
        """
        super(FNNAgent, self).__init__(FNNAgent.TYPE, agent_name, rows, cols, cells)
        self.init_model()
               
    def init_model(self):
        """
        モデルを初期化する。
        """
        
    def train(self):
        """
        学習データセットにより学習を行う。
        """
        
        
    def select_action(self, board) -> np.ndarray:
        """
        手に対する確立を取得する。
        @param board 盤面
        @return 手に対する確率
        """
        result = self.sess.run(self.out_softmax, feed_dict={self.input_boards: [board]})
        return result[0]