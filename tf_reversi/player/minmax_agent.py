# coding: utf-8
""" MinMax法を採用したエージェントに関するモジュールを提供する。 """

from player.player import Player

import copy
import numpy as np
import game.reversi as game

class MinMaxAgent(Player):
    """ MinMax法を採用したエージェントクラスを提供する。 """
    
    # クラス変数 *****************************************************************
    TYPE = 'MINMAX'        # エージェント種別
    INFINITE = 99999999    # 評価値を計算するための先後の基準値
    
    # 下記 要チューニング
    DEPTH_MAX = 4          # 読みの深さ(最大:16 これより大きい値は計算量的に現実的ではない)
    # **************************************************************************
    
    def __init__(self, player_name : str):
        """
        コンストラクタ
        @param player_name プレーヤー名
        """
        super(MinMaxAgent, self).__init__(MinMaxAgent.TYPE, player_name)
        
    def train(self):
        """
        学習を行う。
        継承先クラスにて実装すること。
        """
        print('**************** warning ! ****************')
        print("学習を行う必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')
    
    def select_enable_action(self, env : game.Reversi) -> (float, int):
        """
        手を選択する。
        @param env リバーシゲーム環境
        """
        result_accuracy = 1.0
        result_action = game.PASS
        
        enables = env.get_enables(env.phase)
        if len(enables) == 0:
            return result_accuracy, result_action
        
        if env.phase.is_black():
            evalution, result_action = self.__get_max(env, 0, self.DEPTH_MAX)
        else:
            evalution, result_action = self.__get_min(env, 0, self.DEPTH_MAX)
        
        return result_accuracy, result_action
    
    def is_loadable_model(self):
        """
        モデルの読み込みが可能かを返却する。
        @return 可否
        """
        return False
        
    def load_model(self, model_directory : str):
        """
        モデルを読み込む。 
        @param model_directory モデルの保存先ディレクトリ
        """
        print('**************** warning ! ****************')
        print("モデルを読み込む必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')
        
    def is_savable_model(self):
        """
        モデルが保存可能かを判定する。
        @return 可否
        """
        return False
        
    def save_model(self, sub_dir = None):
        """
        モデルを保存する。
        @param sub_dir サブディレクトリ
        """
        print('**************** warning ! ****************')
        print("モデルの保存が必要ないプレーヤーです。 プレーヤー種別:{}".format(self.PLAYER_TYPE))
        print('*******************************************')
    
    def __get_max(self, env : game.Reversi, depth : int, depth_max : int) -> (int, int):
        """
        評価値がもっとも高い手を選択する。
        @param env リバーシゲーム環境
        @param depth 読みの深さ
        @param depth_max 読みの深さの最大
        @return 評価値, 最善手
        """
        if depth >= depth_max:
            current_eval = env.get_score(env.phase)
            attack = -1 # 最大深度のためこれ以上を先読みを行わないためpassを示す-1を設定
            return current_eval, attack
        
        value = -MinMaxAgent.INFINITE
        attacks = []
        
        enables = env.get_enables(env.phase)
        for enable in enables:
            next_env = copy.deepcopy(env)
            next_env.update(env.phase, enable)
            next_env.change_player()

            next_eval, next_attack = self.__get_min(next_env, depth + 1, depth_max)
            if next_eval > value:
                value = next_eval
                attacks.clear()
                attacks.append((next_eval, enable))
            elif next_eval == value:
                attacks.append((next_eval, enable))
                
        if len(attacks) == 0:
            # 手の候補が空
            return value, -1
        
        index = np.random.choice(len(attacks), 1)
        attack = attacks[int(index)]
        return attack[0], attack[1]
        
    def __get_min(self, env : game.Reversi, depth : int, depth_max : int) -> (int, int):
        """
        評価値がもっとも低くなる手を選択する。
        @param env リバーシゲーム環境
        @param depth 読みの深さ
        @param depth_max 読みの深さの最大
        @return 評価値, 最善手
        """
        if depth >= depth_max:
            current_eval = env.get_score(env.phase)
            attack = -1 # 最大深度のためこれ以上を先読みを行わないためpassを示す-1を設定
            return current_eval, attack
        
        value = MinMaxAgent.INFINITE
        attacks = []
        
        enables = env.get_enables(env.phase)
        for enable in enables:
            next_env = copy.deepcopy(env)
            next_env.update(env.phase, enable)
            next_env.change_player()

            next_eval, next_attack = self.__get_max(next_env, depth + 1, depth_max)
            if next_eval < value:
                value = next_eval
                attacks.clear()
                attacks.append((next_eval, enable))
            elif next_eval == value:
                attacks.append((next_eval, enable))
                
        if len(attacks) == 0:
            # 手の候補が空
            return value, -1
        
        index = 0
        if len(attacks) > 1:
            index = np.random.choice(len(attacks), 1)
        attack = attacks[int(index)]
        return attack[0], attack[1]