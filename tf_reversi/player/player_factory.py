# conding: utf-8
""" プレイヤー生成に関するモジュールを提供する。 """
from player.player import Player
from player.human_player import Human
from player.random_agent import RandomAgent
from player.minmax_agent import MinMaxAgent
from player.perceptron_agent import PerceptronAgent
from player.fnn_agent import FNNAgent
from player.fnn1_agent import FNN1Agent
from player.fnn2_agent import FNN2Agent
from player.cnn_agent import CNNAgent
from player.dqn_agent import DQNAgent
import numpy as np

def create_agent(player_type : str, player_name : str, rows : int, cols : int, cells : np.array) -> Player :
    """
    エージェントを生成する。
    エージェント種別が定義されていないものが指定された場合は、
    RuntimeErrorを発生させる。
    @param agent_type as str エージェント種別
    @return エージェント
    """
    if Human.TYPE == player_type:
        # プレイヤー: 人間
        return Human(player_name)
    
    if RandomAgent.TYPE == player_type:
        # プレイヤー: エージェント(ランダム)
        return RandomAgent(player_name)
    
    if MinMaxAgent.TYPE == player_type:
        # プレイヤー: エージェント(ランダム)
        return MinMaxAgent(player_name)
    
    if PerceptronAgent.TYPE == player_type:
        # プレイヤー: エージェント(パーセプトロン)
        return PerceptronAgent(player_name, rows, cols, cells)
    
    if FNNAgent.TYPE ==  player_type:
        # ハンズオン用エージェント
        return FNNAgent(player_name, rows, cols, cells)
    
    if FNN1Agent.TYPE == player_type:
        # プレイヤー: エージェント(FNN1)
        return FNN1Agent(player_name, rows, cols, cells)
    
    if FNN2Agent.TYPE == player_type:
        # プレイヤー: エージェント(FNN2)
        return FNN2Agent(player_name, rows, cols, cells)
    
    if CNNAgent.TYPE == player_type:
        # プレイヤー: エージェント(FNN2)
        return CNNAgent(player_name, rows, cols, cells)
    
    if DQNAgent.TYPE == player_type:
        # プレイヤー: エージェント(FNN2)
        return DQNAgent(player_name, rows, cols, cells)
    
    raise RuntimeError