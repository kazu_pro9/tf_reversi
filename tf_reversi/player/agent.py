# encoding:utf-8
""" AIエージェントのスーパークラスに関するモジュールです。 """

from abc import abstractmethod
from player.player import Player
from os import path
from os import makedirs
import numpy as np
import tensorflow as tf
import game.reversi as game
import util.path_util as path_util

class Agent(Player):
    """ AIエージェントのスーパークラスを提供する。 """
    
    def __init__(self, agent_type : str, agent_name : str, rows : int, cols : int, cells : np.array):
        """
        コンストラクタ<br />
        継承先から呼び出すこと。
        @param agent_type as str      エージェント種別(MLP, CNN, DQN ...etc)
        @param agent_name as str      エージェント名
        @param rows       as int      盤面の行数
        @param cols       as int      盤面の列数
        @param cells      as nd.array 盤面上のマス一覧
        """
        # スーパークラスのコンストラクタ
        super(Agent, self).__init__(agent_type, agent_name)
        
        # パス設定
        self.model_dir = path_util.get_model_dir(self.player_type, self.player_name)
        self.model_name = "{}.ckpt".format(self.player_name)
        
        # 盤面設定
        self.rows = rows
        self.cols = cols
        self.cells = cells
        self.n_cells = len(self.cells)
    
    @abstractmethod
    def init_model(self):
        """
        モデルを初期化する。
        継承先クラスにて実装すること。
        """
        pass
    
    @abstractmethod
    def train():
        """
        学習を行う。
        継承先クラスにて実装すること。
        """
    
    @abstractmethod
    def select_action(self, board : np.ndarray) -> np.ndarray:
        """
        手に対する確率を取得する。
        @param board 盤面
        @return 各マスへの確率
        """
        result = np.zeros(len(self.enable_actions))
        return result[0]
    
    def select_enable_action(self, env : game.Reversi) -> (float, int):
        """
        手を選択する。
        有効手が存在しない場合は、精度:1/手:PASS(-1)を返却する。
        @param env リバーシゲーム環境
        @return 精度, 手のマスID
        """
        result_action = game.PASS
        result_accuracy = 1
        
        enables = env.get_enables(env.phase)
        if len(enables) < 0:
            return result_accuracy, result_action
            
        accuracies = self.select_action(env.board)
        indexes = np.argsort(accuracies)
        
        for index in reversed(indexes):
            if index in enables:
                result_action = index
                break 
                
        result_accuracy = accuracies[index]       
        return result_accuracy, result_action
    
    def is_loadable_model(self):
        """
        モデルの読み込みが可能かを返却する。
        @return 可否
        """
        return True
    
    def load_model(self, model_directory : str):
        """
        モデルのパラメーター情報を読み込む。
        @param model_directory　モデルのディレクトリ
        """
        if model_directory is None:
            return
        
        checkpoint = tf.train.get_checkpoint_state(model_directory)
        if checkpoint and checkpoint.model_checkpoint_path:
            self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
            
    def is_savable_model(self):
        """
        モデルが保存可能かを判定する。
        @return 可否
        """
        return True
        
    def save_model(self, sub_dir):
        """
        モデルのパラメーター情報を保存する。
        保存先は、 \プロジェクトルート\models\{エージェント種別}\{エージェント名}\{sub_dir}\　となる。
        @param サブディレクトリ
        """
        
        directory = path.join(self.model_dir, sub_dir)
        if not path.exists(directory):
            makedirs(directory)
        
        checkpoint_path = path.join(directory, self.model_name)
        self.saver.save(self.sess, checkpoint_path)