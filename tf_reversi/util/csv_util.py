# coding: utf-8
""" CSV操作に関するモジュールです """

import csv

def write_all_lines(file_path : str, lines : [[]]) :
    """
    ファイルにデータを書き込む。ファイルが既に存在している場合は上書きを行う。
    @param file_path ファイルパス
    @param lines 書き込むデータリスト(行列の2次元リスト)
    """
    with open(file_path, 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        for line in lines:
            writer.writerow(line)

def write_append(file_path : str, lines : [[]]) :
    """
    ファイルの最後に追記する。
    @param file_path ファイルパス
    @param lines 書き込むデータ(行列の2次元リスト)
    """
    with open(file_path, 'a') as f:
        writer = csv.writer(f, lineterminator='\n')
        for line in lines:
            writer.writerow(line)
            
def load_csv(file_path : str) -> [[str]] :
    """
    CSVファイルを読み込み、全データを取得する。
    @param file_path ファイルパス
    @return 行列の2次元リスト
    """
    with open(file_path, 'r') as f:
        reader = csv.reader(f, lineterminator='\n')
        return [line for line in reader]    