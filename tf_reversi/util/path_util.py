# coding: utf-8
""" パス操作に関するユーティリティモジュールを提供する """

from os import path
from os import listdir

# ***************************************************************************************
# 下記定義の相対パスはパッケージルートを基準としている。(__init__.pyが最初に見つかるパス)

# パッケージルート
__PACKAGE_ROOT = '../'

# 棋譜フォルダ
__GAME_RECORD_DIR_REL = '../data/game_records'

# 学習データフォルダ
__TRAIN_GAME_RECORD_DIR_REL = '../data/training_data'

# モデル格納フォルダ
__MODEL_DIR_REL = '../models'

# ログ出力フォルダ
__LOG_DIR_REL = '../logs'

# ***************************************************************************************

def get_root_dir() -> str:
    """
    パッケージルートを取得する。 (__init__.pyが最初に見つかるパス)
    @return パッケージルート
    """
    result = path.normpath(path.join(path.dirname(path.abspath(__file__)), __PACKAGE_ROOT))
    return result

def get_model_dir(agent_type = None, agent_name = None) -> str:
    """
    モデルフォルダを取得する。
    @agent_type as str エージェント種別(MLP/CNN/DQN...etc)
    @agent_name as str エージェント名
    @return モデルフォルダ
    """  
    result = get_abs_path(__MODEL_DIR_REL)
    if not agent_type is None:
        result = path.join(result, agent_type)
        if not agent_name is None:
            result = path.join(result, agent_name)
    return result

def get_log_dir(agent_type = None, agent_name = None) -> str:
    """
    ログフォルダを取得する。
    @return ログフォルダ
    """
    result = get_abs_path(__LOG_DIR_REL)
    if not agent_type is None:
        result = path.join(result, agent_type)
        if not agent_name is None:
            result = path.join(result, agent_name)
    return result

def get_game_record_dir() -> str:
    """
    棋譜フォルダを取得する。
    @return 棋譜フォルダ
    """
    result = get_abs_path(__GAME_RECORD_DIR_REL)
    return result

def get_game_record_file(filename : str) -> str:
    """
    棋譜ファイルのパスを取得する。
    @param　filename ファイル名
    @return ファイルパス
    """
    folder = get_game_record_dir()
    result = path.join(folder, filename)
    return result

def get_train_dir() -> str:
    """
    訓練データフォルダのパスを取得する。
    @return 訓練データフォルダ
    """
    result = get_abs_path(__TRAIN_GAME_RECORD_DIR_REL)
    return result
    
def get_train_file(file_name) -> str:
    """
    訓練データファイルを取得する。
    @param file_name　ファイル名
    @return 訓練データファイルパス
    """
    folder = get_train_dir()
    result = path.join(folder, file_name)
    return result

def get_abs_path(rel_path : str) -> str:
    """
    パッケージルートからの相対パスを絶対パスに変換する。
    @param　rel_path 相対パス
    @return 絶対パス
    """
    root_dir = get_root_dir()
    result = path.normpath(path.join(root_dir, rel_path))
    return result

def exists(abs_path : str) -> bool:
    """
    指定パスが存在するかをチェックする。
    @param abs_path as str パス(絶対パス)
    @return 存在有無
    """
    return path.exists(abs_path)

def enumerate_files(dir_path : str) -> [str]:
    """
    指定フォルダ直下に存在するファイルを列挙する。
    @param dir_path 探索フォルダ
    """
    names = listdir(dir_path)
    files = [path.join(dir_path, name) for name in names]
    result = [file for file in files if path.isfile(file)]
    return result
