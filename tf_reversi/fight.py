# coding: utf-8
""" 対戦に関するモジュールを提供する。 """

from player.player import Player
from player.player_factory import create_agent
import game.reversi as game
import argparse

def fight(black_player : Player, white_player : Player, env : game.Reversi):
    """
    対戦を行う。
    @param black_player 先手プレイヤー
    @param white_player 後手プレイヤー
    @param env リバーシゲーム環境
    """
    if env is None:
        env = game.Reversi()
    env.reset()
    
    print("------------- GAME START ---------------")
    while not env.is_end():
        print("*** 先手プレイヤー ***")
        env.print_screen()
        accuracy, action = black_player.select_enable_action(env)
        if env.is_pass(action):
            print('pass')
        env.update(env.phase, action)
        env.change_player()
        
        print("*** 後手プレイヤー ***")
        env.print_screen()
        accuracy, action = white_player.select_enable_action(env)
        if env.is_pass(action):
            print('pass')
        env.update(env.phase, action)
        env.change_player()
        
    print("*** ゲーム終了 ***")
    env.print_screen()
    
    win = env.winner()
    if win[0] == game.Stone.Black:
        print("あなたの勝ち！ 黒:{:} / 白:{:}".format(win[1], win[2]))
    elif win[0] == game.Stone.White:
        print("あなたの負け！ 黒:{:} / 白:{:}".format(win[1], win[2]))
    else:
        print("ひきわけ 黒:{:} / 白:{:}".format(win[1], win[2]))
        
if __name__ == "__main__":
    # 引数パーサー
    parser = argparse.ArgumentParser()
    parser.add_argument("--black_type", required=True)
    parser.add_argument("--black_name", required=True)
    parser.add_argument("--black_path", required=False)
    parser.add_argument("--white_type", required=True)
    parser.add_argument("--white_name", required=True)
    parser.add_argument("--white_path", required=False)
    args = parser.parse_args()
    
    player1 = create_agent(args.black_type, args.black_name, game.ROWS, game.COLS, game.CELLS)
    if args.black_path is not None and player1.is_loadable_model():
        player1.load_model(args.black_path)
    
    player2 = create_agent(args.white_type, args.white_name, game.ROWS, game.COLS, game.CELLS)
    if args.white_path is not None and player2.is_loadable_model():
        player2.load_model(args.white_path)
    
    fight(player1, player2, None)